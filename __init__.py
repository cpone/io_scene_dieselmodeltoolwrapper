#  Copyright (c) 2021 Cpone contact@connieprice.co.uk
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Plugin Info
bl_info = {
    "name": "Diesel Model Tool Wrapper",
    "author": "Cpone",
    "blender": (2, 80, 0),
    "description": "A simple export and import wrapper for the model tool.",
    "category": "Import-Export"
}

# Reload Modules if Blender is Reloaded
if "bpy" in locals():
    import importlib
    if "preferences" in locals():
        importlib.reload(preferences)
    if "helpers" in locals():
        importlib.reload(helpers)
    if "model" in locals():
        importlib.reload(model)

import bpy
this_is_the_good_blender = (bpy.app.version < (2, 80))

from bpy.props import PointerProperty

# If we ain't in 2.8 then it's rewind time.
if this_is_the_good_blender:
    bl_info["blender"] = (2, 74, 0)

from . import helpers, preferences, model

# Basic Menu Functions
def menu_func_import(self, context):
    self.layout.operator(model.DieselModelToolWrapperImporter.bl_idname, text="Diesel Model (.model)")

def menu_func_export(self, context):
    self.layout.operator(model.DieselModelToolWrapperExporter.bl_idname, text="Diesel Model (.model)")

# Class Registration
classes = (
    preferences.DieselModelToolWrapperPreferences,
    preferences.OT_DieselGLTFModelToolBrowser,
    preferences.OT_DieselFBXModelToolBrowser,
    model.DieselModelToolWrapperImporter,
    model.DieselModelToolWrapperExporter
)

def register():
    for cls in classes:
        helpers.make_annotations(cls)
        bpy.utils.register_class(cls)

    if this_is_the_good_blender:
        bpy.types.INFO_MT_file_import.append(menu_func_import)
        bpy.types.INFO_MT_file_export.append(menu_func_export)
    else:
        bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
        bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    if this_is_the_good_blender:
        bpy.types.INFO_MT_file_import.remove(menu_func_import)
        bpy.types.INFO_MT_file_export.remove(menu_func_export)
    else:
        bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
        bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    for cls in classes:
        bpy.utils.unregister_class(cls)