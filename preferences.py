import bpy

from bpy_extras.io_utils import ImportHelper
from bpy.types import Operator, AddonPreferences
from bpy.props import StringProperty

from . import helpers

class DieselModelToolWrapperPreferences(AddonPreferences):
	bl_idname = __package__

	gltf_model_tool_path = StringProperty(
		name="GLTF Model Tool Path"
	)

	fbx_model_tool_path = StringProperty(
		name="FBX Model Tool Path"
	)

	def draw(self, context):
		layout = self.layout

		layout.prop(self, "gltf_model_tool_path")
		layout.operator("io_scene_dieselmodeltoolwrapper.locate_gltf_model_tool")
		layout.prop(self, "fbx_model_tool_path")
		layout.operator("io_scene_dieselmodeltoolwrapper.locate_fbx_model_tool")

class OT_DieselGLTFModelToolBrowser(Operator, ImportHelper):
	bl_idname = "io_scene_dieselmodeltoolwrapper.locate_gltf_model_tool"
	bl_label = "Locate GLTF Model Tool (.exe)"

	filter_glob: StringProperty(
		default='*.exe',
		options={'HIDDEN'}
	)

	def execute(self, context):
		helpers.set_property("gltf_model_tool_path", self.filepath)
		return {'FINISHED'}

class OT_DieselFBXModelToolBrowser(Operator, ImportHelper):
	bl_idname = "io_scene_dieselmodeltoolwrapper.locate_fbx_model_tool"
	bl_label = "Locate FBX Model Tool (.exe)"

	filter_glob: StringProperty(
		default='*.exe',
		options={'HIDDEN'}
	)

	def execute(self, context):
		helpers.set_property("fbx_model_tool_path", self.filepath)
		return {'FINISHED'}