from . import helpers

import os.path, tempfile, subprocess, mathutils, math

import bpy
from bpy.props import (BoolProperty, StringProperty)
from bpy_extras.io_utils import (ImportHelper, ExportHelper)

class DieselModelToolWrapperImporter(bpy.types.Operator, ImportHelper):
	bl_idname = "import_scene.diesel_model"
	bl_label = "Import Diesel Model"
	bl_description = "Import a Diesel Model"
	bl_options = {'UNDO'}

	filename_ext = ".model"
	filter_glob = StringProperty(default = "*.model", options = {'HIDDEN'})

	correct_axes = BoolProperty(
		name="Correct Axes (Buggy on anything complex because I'm lazy.)",
		default=False
	)

	def execute(self, context):
		model_import(self.filepath, self.properties.correct_axes)

		return {'FINISHED'}

	def invoke(self, context, event):
		bpy.context.window_manager.fileselect_add(self)

		return {'RUNNING_MODAL'}

meshfix = mathutils.Matrix.Rotation(math.radians(-90), 4, 'X')
def model_import(filepath, correct_axes=False):
	gltf_model_tool_path = helpers.get_property("gltf_model_tool_path")
	if gltf_model_tool_path == None or not os.path.isfile(gltf_model_tool_path):
		self.report({"WARNING"}, "You haven't set a valid model tool path in the addon preferences!")
		return {'CANCELLED'}

	with tempfile.TemporaryDirectory() as temp_dir_name:
		gltf_path = temp_dir_name + "\\diesel_model.glb"
		subprocess.call([
			gltf_model_tool_path,
			"--load={}".format(filepath),
			"--export={}".format(gltf_path)
		])

		bpy.ops.import_scene.gltf(
			filepath=gltf_path,
			bone_heuristic='BLENDER'
		)

		if correct_axes:
			new_objects = bpy.context.selected_objects[:]

			for obj in new_objects:
				obj.location = (obj.location[0], obj.location[2], -obj.location[1])

				if obj.data:
					obj.data.transform(meshfix)

					if (isinstance(obj.data, bpy.types.Mesh)):
						obj.data.update()

class DieselModelToolWrapperExporter(bpy.types.Operator, ExportHelper):
	bl_idname = "export_scene.diesel_model"
	bl_label = "Export Diesel Model"
	bl_description = "Export a Diesel Model"

	filename_ext = ".model"
	filter_glob = StringProperty(default = "*.model", options = {'HIDDEN'})

	only_selected = BoolProperty(
		name="Only Export Selected",
		default=False
	)

	apply_modifiers = BoolProperty(
		name="Apply Modifiers",
		default=False
	)

	correct_axes = BoolProperty(
		name="Correct Axes",
		default=False
	)

	def execute(self, context):
		model_export(self.filepath, self.properties.only_selected, self.properties.apply_modifiers, self.properties.correct_axes)

		return {'FINISHED'}

	def invoke(self, context, event):
		bpy.context.window_manager.fileselect_add(self)

		return {'RUNNING_MODAL'}

pre_32_changes = (bpy.app.version < (3, 2))
def model_export(filepath, only_selected=False, apply_modifiers=False, correct_axes=True):
	gltf_model_tool_path = helpers.get_property("gltf_model_tool_path")
	if gltf_model_tool_path == None or not os.path.isfile(gltf_model_tool_path):
		self.report({"WARNING"}, "You haven't set a valid GLTF model tool path in the addon preferences!")
		return {'CANCELLED'}

	with tempfile.TemporaryDirectory() as temp_dir_name:
		gltf_path = temp_dir_name + "\\diesel_model.glb"

		if pre_32_changes:
			bpy.ops.export_scene.gltf(
				filepath=gltf_path,
				export_selected=only_selected,
				export_apply=apply_modifiers,
				export_yup=not correct_axes,
				# export_tangents=True
			)
		else:
			bpy.ops.export_scene.gltf(
				filepath=gltf_path,
				use_selection=only_selected,
				export_apply=apply_modifiers,
				export_yup=not correct_axes,
				# export_tangents=True
			)

		subprocess.call([
			gltf_model_tool_path,
			"--new",
			"-n+",
			"--import={}".format(gltf_path),
			"--save={}".format(filepath)
		])